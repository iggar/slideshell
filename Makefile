PREFIX ?= /usr
MANDIR ?= $(PREFIX)/share/man
CFGDIR ?= /etc

all:
	@echo Run \'make install\' to install Slideshell.

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@mkdir -p $(DESTDIR)$(MANDIR)/man1
	@cp -p slideshell $(DESTDIR)$(PREFIX)/bin/slideshell
	@cp -p slideshell.1 $(DESTDIR)$(MANDIR)/man1
	@cp -p slideshellrc $(CFGDIR)/slideshellrc
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/slideshell

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/slideshell
	@rm -rf $(DESTDIR)$(MANDIR)/man1/slideshell.1*
	@rm -rf $(CFGDIR)/slideshellrc
