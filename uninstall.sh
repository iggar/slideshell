#!/usr/bin/env bash

[[ $EUID -ne 0 ]] && echo -e "\nTry: sudo ./uninstall.sh\n" && exit 1

printf "\nDesinstalando..."

rm /usr/bin/slideshell
rm /etc/slideshellrc
rm /usr/share/man/man1/slideshell.1.gz
rm -r /usr/share/doc/slideshell

printf "Pronto!\n"

exit
